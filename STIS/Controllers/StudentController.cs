﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STIS.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DropStudent()
        {
            return View();
        }

        public ActionResult DropDuplicateStudent()
        {
            return View();
        }

      
        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult DoublePromotion()
        {
            return View();
        }
        public ActionResult VerifiedStudents()
        {
            return View();
        }
        public ActionResult EnrollmentDetails()
        {
            return View();
        }
        public ActionResult NonVerified_Students()
        {
            return View();
        }
        public ActionResult DuplicateStudents()
        {
            return View();
        }
        public ActionResult ViewDropout()
        {
            return View();
        }
        public ActionResult Inclusive()
        {
            return View();
        }

        public ActionResult InvalidCNIC()
        {
            return View();
        }
        
        public ActionResult NoPictures()
        {
            return View();
        }
    }
}