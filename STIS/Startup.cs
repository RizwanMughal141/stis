﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(STIS.Startup))]
namespace STIS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
